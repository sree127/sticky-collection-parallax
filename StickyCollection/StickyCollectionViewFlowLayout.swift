//
//  StickyCollectionViewFlowLayout.swift
//  StickyCollection
//
//  Created by Sreejith on 02/05/17.
//  Copyright © 2017 Cognitive. All rights reserved.
//

import UIKit

class StickyCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    /// Bool to check Grid.List
    var isGridLayout = false
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
 
    func setupLayout() {
        
        minimumInteritemSpacing = 1
        minimumLineSpacing = 1
        scrollDirection = .vertical
    }
    
    /// Cell Item Width Calculation
    func itemWidth() -> CGFloat {
        return isGridLayout ? (collectionView!.frame.width / 3) - 1 : collectionView!.frame.width
    }
    
    override var itemSize: CGSize {
        
        set { self.itemSize = CGSize(width: itemWidth(), height: 125) }
        
        get { return CGSize(width: itemWidth(), height: 125) }
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
    
    /// Sticky Header 
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        guard let layoutAttributes = super.layoutAttributesForElements(in: rect) else { return nil }
        
        let sectionsToAdd = NSMutableIndexSet()
        
        var newLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for layoutAttributesSet in layoutAttributes {
            
            if layoutAttributesSet.representedElementCategory == .cell {
                newLayoutAttributes.append(layoutAttributesSet)
                sectionsToAdd.add(layoutAttributesSet.indexPath.section)
                
            } else if layoutAttributesSet.representedElementCategory == .supplementaryView {
                sectionsToAdd.add(layoutAttributesSet.indexPath.section)
            }
        }
        
        for section in sectionsToAdd {
            
            let indexPath = IndexPath(item: 0, section: section)
            
            if let sectionAttributes = self.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: indexPath) {
                newLayoutAttributes.append(sectionAttributes)
            }
        }
        
        return newLayoutAttributes
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        guard let layoutAttributes = super.layoutAttributesForSupplementaryView(ofKind: elementKind, at: indexPath) else { return nil }
        
        if indexPath.section != 0 {
            
            guard let boundaries = boundaries(forSection: indexPath.section) else { return layoutAttributes }
            guard let  colletionViewMain = collectionView else { return layoutAttributes }
            
            let contentOffsetY = colletionViewMain.contentOffset.y
            var frameForSupplementaryView = layoutAttributes.frame
            
            let minimum = boundaries.minimum - frameForSupplementaryView.height
            let maximum = boundaries.maximum - frameForSupplementaryView.height
            
            if contentOffsetY < minimum {
                frameForSupplementaryView.origin.y = minimum
            } else if contentOffsetY > maximum {
                frameForSupplementaryView.origin.y = maximum
            } else {
                frameForSupplementaryView.origin.y = contentOffsetY
            }
            
            layoutAttributes.frame = frameForSupplementaryView
            
            return layoutAttributes
            
        } else { return layoutAttributes }
    }

    func boundaries(forSection section : Int) -> (minimum : CGFloat, maximum : CGFloat)? {
        
        var result = (minimum : CGFloat(0.0), maximum : CGFloat(0.0))
        
        guard let collectionView = collectionView else { return result }
        
        let numberOfItems = collectionView.numberOfItems(inSection: section)
        
        guard numberOfItems > 0  else {
            return result
        }
        
        if let firstItem = layoutAttributesForItem(at: IndexPath(item: 0, section: section)),
            let lastItem = layoutAttributesForItem(at: IndexPath(item: numberOfItems - 1, section: section)) {
            
            result.minimum = firstItem.frame.minY
            result.maximum = lastItem.frame.maxY
            
            result.minimum -= headerReferenceSize.height
            result.maximum -= headerReferenceSize.height
            
            result.minimum -= sectionInset.top
            result.maximum += (sectionInset.top + sectionInset.bottom)

         }
        
        return result
    }
}

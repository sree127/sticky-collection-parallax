//
//  ViewController.swift
//  StickyCollection
//
//  Created by Sreejith on 02/05/17.
//  Copyright © 2017 Cognitive. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIViewControllerPreviewingDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    /// Identifiers
    fileprivate let CellIdentifier = "Cell"
    fileprivate let HeaderIdentifier = "Header"
    fileprivate let ReusabelView = "ReusableView"

    /// Bool to check whether the current CollectionView layout is Grid or List
    var isPhotos = true
    
    /// Initialising the Flow Layout
    var flowLayout = StickyCollectionViewFlowLayout()
    
    /// Constants for calculating the Parallax effect
    enum Constant : CGFloat  {
        case ParallaxOffSetSpeed = 30
        case CellHeight = 125
    }

    /// Constants for XIB namess
    enum HeaderXIBs : String {
        case StickyHeader = "StickyHeader"
        case RootHeaderView = "IGView"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false

        collectionView.delegate = self
        collectionView.dataSource = self
        
        /// Registering for 3D Touch
        registerForPreviewing(with: self, sourceView: collectionView)

        /// Set CollectionViewFlowLayout
        flowLayout.isGridLayout = true
        collectionView.setCollectionViewLayout(flowLayout, animated: true)
        
        /// Initialising 2 Headers for Coollection View
        let xib = UINib.init(nibName: HeaderXIBs.StickyHeader.rawValue, bundle: Bundle.main)
        collectionView.register(xib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: HeaderIdentifier)
        
        let reusableView = UINib.init(nibName: HeaderXIBs.RootHeaderView.rawValue, bundle: Bundle.main)
        collectionView.register(reusableView, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ReusabelView)
    }
    
    
    // MARK:- 3D Touch Delegates
    
    /// Delegate methods
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        /// Get the IndexPath force-touched
        guard let indexPath = collectionView.indexPathForItem(at: location), let cell = collectionView.cellForItem(at: indexPath) else { return nil }
        
        /// This will blur the entire screen except for the cell.frame
        previewingContext.sourceRect = cell.frame

        /// View Controller to Pop
        let popNavController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopNavController") as! UINavigationController
        let pop = popNavController.viewControllers.first as! PopViewController
        pop.image = isPhotos ? "square" : "bgPNG"
        return popNavController
    }
    
    /// Present the viewController after force-touch
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        present(viewControllerToCommit, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK:- Data Source
/// Data Source
extension ViewController : UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? 0 : 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        /// Dequeue Reusable Cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier, for: indexPath) as! CollectionCell
        
        /// Configure Cell
        cell.backgroundColor = UIColor(red:0.2, green:0.25, blue:0.3, alpha:1.0)
        cell.contentView.backgroundColor = UIColor(red:0.2, green:0.25, blue:0.3, alpha:1.0)
        
        cell.imageView.image = isPhotos ? UIImage(named: "square") : UIImage(named: "bgPNG") /// Dummy Images
        
        /// Setting Constraints for Parallax
        cell.imageViewTopConstraint.constant = parallaxOffset(newOffsetY: collectionView.contentOffset.y, cell: cell)
        cell.imageViewHeight.constant = parallaxImageHeight
        
        cell.titleLabel.isHidden = isPhotos ? true : false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        /// Init the Sticky Header From Here
        switch indexPath.section {
            
        case 1:
            if let supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: HeaderIdentifier, for: indexPath) as? StickyHeader {

                /// Button Press Callback from Sticky Header
                supplementaryView.buttonPressed  = { (sender) in
                
                    let button = sender as! UIButton
                    self.isPhotos = button.tag == 101 ? true : false
                    self.flowLayout.isGridLayout = button.tag == 101 ? true : false
                    
                    /// Animate the Grid-List or Vice versa and reload the visible cells
                    UIView.animate(withDuration: 0.3, animations: {
                        collectionView.collectionViewLayout.invalidateLayout()
                        collectionView.setCollectionViewLayout(self.flowLayout, animated: true)
                    }, completion: { (value) in
                       let visibleCellsArray = collectionView.indexPathsForVisibleItems
                        collectionView.reloadItems(at: visibleCellsArray)
                    })
                }
                
                return supplementaryView
            }
            
        case 0:
            if let supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ReusabelView, for: indexPath) as? IGView { return supplementaryView }
            
        default:
            if let supplementaryView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ReusabelView, for: indexPath) as? IGView { return supplementaryView }
        }
        
        fatalError("Unable to Dequeue Reusable Supplementary View")
    }
    
    /// Logic where the parallax property is calculated
    var parallaxImageHeight : CGFloat {
        
        let maxOffset = (sqrt(pow(Constant.CellHeight.rawValue, 2) + 4 * Constant.ParallaxOffSetSpeed.rawValue * collectionView.frame.height) - Constant.CellHeight.rawValue) / 2
        return maxOffset + Constant.CellHeight.rawValue
    }
    
    func parallaxOffset(newOffsetY : CGFloat, cell : UICollectionViewCell) -> CGFloat {
        return (newOffsetY - cell.frame.origin.y) / parallaxImageHeight * Constant.ParallaxOffSetSpeed.rawValue
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        /// Set the parallax offset while on scroll
        let offsetY = collectionView.contentOffset.y
        for visibleCell in collectionView.visibleCells as! [CollectionCell] {
            visibleCell.imageViewTopConstraint.constant = parallaxOffset(newOffsetY: offsetY, cell: visibleCell)
        }
    }
}

// MARK:- Delegate Methods
///
extension ViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Collection View Delegate Flow Layout Methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return  section == 0 ? CGSize(width: collectionView.bounds.width, height: 240.0) : CGSize(width: collectionView.bounds.width, height: 50)
    }
}


// MARK:- CollectionView Cell Subclass
///
class CollectionCell : UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
    }
}

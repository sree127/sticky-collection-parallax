//
//  PopViewController.swift
//  StickyCollection
//
//  Created by Sreejith on 03/05/17.
//  Copyright © 2017 Cognitive. All rights reserved.
//

import UIKit

class PopViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var image : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.image = UIImage(named: image ?? "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func cancelPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

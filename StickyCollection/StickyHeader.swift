//
//  StickyHeader.swift
//  StickyCollection
//
//  Created by Sreejith on 02/05/17.
//  Copyright © 2017 Cognitive. All rights reserved.
//

import UIKit

class StickyHeader: UICollectionReusableView {

    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    
    /// Callback for Button Press
    var buttonPressed : ((Any) -> ())?
    
    override func awakeFromNib() {
        button1.tag = 101
        button2.tag = 202
    }
    

    @IBAction func button2Pressed(_ sender: Any) {
        buttonPressed?(sender)
    }

    @IBAction func pressed(_ sender: Any) {
        buttonPressed?(sender)
    }
}
